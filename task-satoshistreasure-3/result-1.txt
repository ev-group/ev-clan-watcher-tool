<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,Chrome=1,requiresActiveX=true" />
<link rel="stylesheet" type="text/css" href="/stylesheets/style.css">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/images/site.webmanifest">
<link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138310651-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-138310651-1');
</script>
<title>Satoshi's Treasure</title>
</head>
<body>
<div class="language-picker">
  <a href=?lng=en>EN </a>|<a href=?lng=zh> ZH</a>
</div>
<div class="container">

<pre>
<a href="/" style='text-decoration:none;'>
 _____       _            _     _ _       _____
/  ___|     | |          | |   (_| )     |_   _|
\ `--.  __ _| |_ ___  ___| |__  _|/ ___    | |_ __ ___  __ _ ___ _   _ _ __ ___
 `--. \/ _` | __/ _ \/ __| '_ \| | / __|   | | '__/ _ \/ _` / __| | | | '__/ _ \
/\__/ / (_| | || (_) \__ \ | | | | \__ \   | | | |  __/ (_| \__ \ |_| | | |  __/
\____/ \__,_|\__\___/|___/_| |_|_| |___/   \_/_|  \___|\__,_|___/\__,_|_|  \___|

</a>
</pre>
  <div class="box-pre">
    <h3>研究助理</h3>
    <p class="line-through">在寻找钥匙的过程中，有时，你也许会被要求为一些看不见的人群提供帮助，而这些人的任务，是确保所有的游戏都可以正常运行。此刻，本组织的三名成员就需要你的帮助，来找到一些非常具体的物件，并完成一个特别的任务。</p>
    <p class="line-through">然而，组织的三名成员都非常受规则约束，因此请务必遵守以下要求。当你找到成员后，请接近他们并问“对不起，你是在寻找这个吗？”，并用双手将物体提供给他们，成员将为你提供独特的钥匙作为奖励。请不要试图以任何其他方式与成员进行交谈或互动。但如果你尝试接近某人，并向他们提出这个问题，他们却似乎不知道你在说什么，你就可以确信你找错人了。那么抱歉，请继续你的搜索。</p>
    <p class="line-through">你的目标:</p>
<a class="line-through" href="/images/agent1.jpg">成员一</a>
<a class="line-through" href="/images/agent2_.jpg">成员二</a>
<a class="line-through" href="/images/agent3.jpg">成员三</a>

    <p>本线索已经失效，猎之钥已经被找到，余下成员二和成员三持有的额外的密钥已经被回收到密钥池中</p>
    <h5></h5>
  </div>
</div>
<script>
var w1 = document.querySelector('.container').clientWidth;
var w2 = window.screen.availWidth;
var meta = document.createElement('meta');
meta.setAttribute('name', 'viewport');
meta.setAttribute('content', 'width=device-width,initial-scale=' + (w2/w1));
document.getElementsByTagName('head')[0].appendChild(meta);
</script>
</body>
</html>
