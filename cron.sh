#!/bin/sh

GIT=`which git`
REPO_DIR=/root/.watchme/watcher
cd ${REPO_DIR}

#Pull from remote
${GIT} pull origin master

#Run Watcher
/usr/local/bin/watchme run watcher # watchme-watcher

#Track empty folders
find . -not -path '*/\.*' -type d -exec sh -c 'for x;do ls -1q "$x" | wc -l | g\
rep -q "\b1\b" && touch "$x/.gitkeep" && git add "$x/.gitkeep" && git commit -m\
 "add empty directory "$x"";done' _ {} +

#Push to remote
${GIT} push origin master
